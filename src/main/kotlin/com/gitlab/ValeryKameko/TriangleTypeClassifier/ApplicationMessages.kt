package com.gitlab.ValeryKameko.TriangleTypeClassifier

import java.util.*

object ApplicationMessages {
    private val resourceBundle: ResourceBundle =
        ResourceBundle.getBundle("ApplicationMessages", Locale.getDefault())

    fun getMessage(message: String): String = resourceBundle.getString(message)
}