package com.gitlab.ValeryKameko.TriangleTypeClassifier

import java.math.BigInteger
import java.util.*
import kotlin.system.exitProcess

object Application {
    private val POISON_MESSAGE = "q"
    private val PROMPT_MESSAGES = listOf("enterFirstLength", "enterSecondLength", "enterThirdLength")
    private val RANGE = BigInteger.ONE..9223372036854775807.toBigInteger()

    private val scanner = Scanner(System.`in`)
    private val runtime = Runtime.getRuntime()

    private fun clearScreen() {
       if (System.getProperty("os.name").toLowerCase().contains("win")) {
           ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor()
       } else {
           runtime.exec("clear")
       }
    }

    private fun readLine(): String {
        val line = scanner.nextLine()
        if (line == POISON_MESSAGE) exitProcess(0)
        return line
    }

    private fun readBigInteger(message: String, range: ClosedRange<BigInteger>? = null): BigInteger {
        do {
            clearScreen()
            print(message)

            val value = readLine().toBigIntegerOrNull()
            if (value?.let { range?.contains(it) != false } == true) return value

            print(String.format(ApplicationMessages.getMessage("incorrectValue"), range.toString()))
            readLine()
        } while (true)
    }

    fun run() {
        do {
            val lengths = PROMPT_MESSAGES.map { readBigInteger(ApplicationMessages.getMessage(it), RANGE) }
            val triangle = Triangle(Triple(lengths[0], lengths[1], lengths[2]))

            if (!triangle.valid()) {
                print(ApplicationMessages.getMessage("incorrectTriangle"))
                readLine()
                continue
            }

            print(String.format(
                ApplicationMessages.getMessage("triangleKindIs"),
                ApplicationMessages.getMessage(triangle.kind.value)))
            readLine()
        } while (true)
    }
}