package com.gitlab.ValeryKameko.TriangleTypeClassifier

import java.math.BigInteger

class Triangle(edges: Triple<BigInteger, BigInteger, BigInteger>) {
    enum class Kind(val value: String) {
        Equilateral("equilateralKind"),
        Isosceles("isoscelesKind"),
        Scalene("scaleneKind"),
    }

    private val edges = edges.toList().sorted()

    val kind
        get() = when (edges.distinct().count()) {
            1 -> Kind.Equilateral
            2 -> Kind.Isosceles
            else ->  Kind.Scalene
        }

    fun valid() = edges[2] < edges[0] + edges[1]
}